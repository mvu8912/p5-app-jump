package JMP::Config;

#------------------------------------------------------------------------------------------------
#
# Config.pm 
#
# Description:  Inspired by Mojolicious::Plugin::Config - simple, fast perl-based config management
#
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW 
#
#------------------------------------------------------------------------------------------------

use Mojo::Base -base;

use File::HomeDir;
use Path::Tiny;


has 'config' 			=> sub { shift->load };

has 'config_contents' 	=> sub {

    my ($self) = @_;

    $self->create_config_file unless -e $self->config_location;

    return path($self->config_location)->slurp;

};

has 'config_location' 	=> sub { File::HomeDir->my_home . '/.jmp.conf' };


sub get {
	
	my ($self) = @_;

	return $self->config;

}


sub load {

    my ($self) = @_;

    my $contents = $self->config_contents;

    # run perl to return a hash
    my $config = eval "package JMP::Config::Sandbox; no warnings; use Mojo::Base -strict; $contents";

    # IDEA: consider using the "app" super sub later (see below)
    #my $config
    #  = eval 'package Mojolicious::Plugin::Config::Sandbox; no warnings;'
    #  . "sub app; local *app = sub { \$app }; use Mojo::Base -strict; $content";

    if (my $error = $@) {
        return $self->_show_error("Config parsing error: " . $error);
    }

    return $self->_show_error('Configuration file did not return a hash reference.')
        unless ref $config eq 'HASH';

    return $config;

}


sub _show_error {

    my ($self, $error) = @_;

    say "Configuration file error: " . $self->config_location;
    say $error;

}


sub create_config_file {

    my ($self) = @_;

    my $default_config = <<CONFIG;
#----------------------------------------------------------------------------
#
# .jmp-config - store config for individual commands here (perl syntax)
#
#---------------------------------------------------------------------------  

{
  	commands 	=> {
	  	db		=>	{
  			shell 	=>  'mysql',	# connect to your local database
  		},
	    edit 	=> 	{
      		editor => 'subl [-filename-]:[-line_number-]',
    	},   
	}
}
    
CONFIG

    path($self->config_location)->spew($default_config);

}

1;
