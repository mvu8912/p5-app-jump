package JMP::Screen::Footer;

#------------------------------------------------------------------------------------------------
#
# Footer.pm 
#
# Description:  Show a footer to the page including actions
#
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW 
#
#------------------------------------------------------------------------------------------------

use JMP::Object -base;
use JMP::Template;

has actions  => sub { [] };	

sub render_actions {
	
	my ($self) = @_;

	my $rendered_actions = '';
	my $key_actions 	 = {};

	my @numbers = 0 .. 9;

	foreach my $action (@{$self->actions}) {
	
		my $assigned_key = shift @numbers;
		
		$rendered_actions .= '[' . $assigned_key . ']' . $action->{text} . '  '; 
		
		$key_actions->{$assigned_key} = $action->{action};
	
	}

	return ($rendered_actions, $key_actions);
	
}

1;
