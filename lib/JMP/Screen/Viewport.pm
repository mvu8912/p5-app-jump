package JMP::Screen::Viewport;

#------------------------------------------------------------------------------------------------
#
# Viewport.pm 
#
# Description:  Show paginated output consistently across different commands.
#
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW 
#
#------------------------------------------------------------------------------------------------

use Data::Page;
use JMP::Object -base;
use JMP::Template;
use JMP::Screen::Keys;

# the Viewport::ActionTemplate(s) set by the JMP::Command
has action_templates 			=> 	sub { [] };	
has action_templates_per_page	=>	12;

# page level actions
has page_actions 				=>	sub { [] };

# break up the results into pages
has pager 						=> 	sub {
	
	my ($self) = @_;

    my $pager = Data::Page->new;
    $pager->total_entries(scalar(@{$self->action_templates}));
    $pager->entries_per_page($self->action_templates_per_page);	
    $pager->current_page(1);
    return $pager;
	
};


sub current_page {
	
	my ($self) = @_;

	return $self->pager->current_page;

}


sub render {
	
	my ($self, $current_page) = @_;

	$current_page = $current_page // 1;

	$self->pager->current_page($current_page);

	# get the latest page full of actions
	my @page_actions = $self->pager->splice($self->action_templates);

	# assign keys to the page actions
	my $key_actions = $self->_assign_page_action_keys(\@page_actions);
	
	# render the viewport actions
	my $rendered_actions = join(' ', map { $_->render } @page_actions);

	return ($rendered_actions, $key_actions);

}


sub last_page {
	
	my ($self) = @_;

	return $self->pager->last_page;

}


sub next_page {
	
	my ($self) = @_;

	return $self->pager->next_page;

}


sub previous_page {
	
	my ($self) = @_;

	return $self->pager->previous_page;

}


sub _assign_page_action_keys {

	my ($self, $page_actions) = @_;
    
    return unless $page_actions;
    
	my $page_keys = JMP::Screen::Keys->new;
	
	my $key_actions = {};

	foreach my $page_action (@{$page_actions}) {

		my $uppercase_command = $page_action->uppercase_key_command;
		my $lowercase_command = $page_action->lowercase_key_command;

		# assign keys to the action templates
		if ($uppercase_command) {
			my $uppercase_key = $page_keys->get_next_uppercase_key;
			$page_action->uppercase_key($uppercase_key);
			$key_actions->{$uppercase_key} = $uppercase_command;
		}
		if ($lowercase_command) {
			my $lowercase_key = $page_keys->get_next_lowercase_key;
			$page_action->lowercase_key($lowercase_key);
			$key_actions->{$lowercase_key} = $lowercase_command;
		}
		
	}

	return $key_actions;

}

1;
