package JMP::Screen::Keys;

#------------------------------------------------------------------------------------------------
#
# Keys.pm 
#
# Description:  Hand out action keys
#
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW 
#
#------------------------------------------------------------------------------------------------

use JMP::Object -base;

# the next actions set by the JMP::Command
has lowercase_keys => sub { [ 'a' .. 'z' ] }; 
has number_keys    => sub { [ '1' .. '9' ] };
has uppercase_keys => sub { [ 'A' .. 'Z' ] };


sub get_next_lowercase_key {

	my ($self) = @_;
	
	my $lowercase_key = shift @{$self->lowercase_keys};
	
	die "No more lowercase keys available. Reduce the number of templates per page." 
		unless $lowercase_key;

	return $lowercase_key;

}


sub get_next_number_key {

	my ($self) = @_;
	
	my $number_key = shift @{$self->number_keys};
	
	die "No more number_keys available. Reduce the number of numbers per page." 
		unless $number_key;
	
	return $number_key;
	
}


sub get_next_uppercase_key {

	my ($self) = @_;
	
	my $uppercase_key = shift @{$self->uppercase_keys};
	
	die "No more uppercase keys available. Reduce the number of templates per page." 
		unless $uppercase_key;
	
	return $uppercase_key;
	
}

1;
