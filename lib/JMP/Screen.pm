package JMP::Screen;

#------------------------------------------------------------------------------------------------
#
# Screen.pm 
#
# Description:  Show paginated screens of output consistently across different commands.
#
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW 
#
#------------------------------------------------------------------------------------------------

use JMP::Object -base;
use JMP::Template;
use JMP::Screen::Footer;
use JMP::Screen::Viewport;
use Term::ANSIColor qw(:constants);
use Term::ReadKey;

# the Screen::ActionTemplate(s) set by the JMP::Command
has action_templates 		=> 	sub { [] };	

# page level actions
has global_actions 			=>	sub { [] };

has screen_template 		=>	sub { shift->_build_screen_template };			

# set the title in the JMP::Command
has title 					=> 	'SET TITLE IN YOUR COMMAND';

has footer => sub {
	JMP::Screen::Footer->new(
		actions => shift->global_actions,
	);
};


has viewport => sub {

	my ($width, $height_in_lines) = Term::ReadKey::GetTerminalSize();
	JMP::Screen::Viewport->new(
		action_templates => shift->action_templates,
		line_height 	 => $height_in_lines - 8,	# take 8 off header and footer  
	);
};


sub display_page {
	
	my ($self, $current_page) = @_;

    system("clear");
	
    my ($viewport, $viewport_key_actions) = $self->viewport->render($current_page);
    my ($footer, $footer_key_actions) 	  = $self->footer->render_actions;

	# show the screen with paginated actions
	say $self->_render_page($viewport, $footer);
	
	# over to the user !
    $self->_prompt_user($viewport_key_actions, $footer_key_actions);

}


sub _build_screen_template {

	my ($self) = @_;

	return <<TEMPLATE;
	
[-title-]						
_____________________________________________________________________________________
                                                                     [-current_page-] of [-total_pages-]
[-viewport-]
                                [-back_arrow-] [-next_arrow-]
_____________________________________________________________________________________
[-footer-] 
TEMPLATE

}


sub _do_action {

	my ($self, $key, $key_actions) = @_;

	my $command = $key_actions->{$key};

	die "No command found for key: $key" unless $command;

#	say $command;
	
#	exit;

	system($command);

}


sub _get_key_old {

    my ($self) = @_;

    my $key;

    ReadMode 4; # Turn off controls keys
    while (not defined ($key = ReadKey(-1))) {
        # No key yet
    }
    ReadMode 0; # Reset tty mode before exitin
    return $key;

}


sub _get_key {

    # see recipe 15.6 Perl Cookbook
#    ReadMode('cbreak');
    ReadMode('raw');
    my $char = ReadKey(0);
    ReadMode('normal');
    return $char;

}


sub _prompt_user {

    my ($self, $key_actions, $footer_key_actions) = @_;

    while (my $key = $self->_get_key) {

		if ($key =~ m/^[a-zA-Z]$/) {

        	$self->_do_action($key, $key_actions);
        	
        	# redisplay the current page
        	$self->display_page($self->viewport->current_page);
        
        }
		elsif ($key =~ m/^[0-9]$/) {

        	$self->_do_action($key, $footer_key_actions);

        	# redisplay the current page
        	$self->display_page($self->viewport->current_page);
        	
		}
      	elsif ($key eq '<') {

        	# go to the previous page
        	if (my $previous_page = $self->viewport->previous_page) {
        		$self->display_page($previous_page);
        	}
        }
        elsif ($key eq '>') {

        	# go to the next page
        	if (my $next_page = $self->viewport->next_page) {
        		$self->display_page($next_page);
        	}
        }
        elsif ($key eq '' or $key eq '0') {
        	exit;
        }

    }
}


sub _render_page {

	my ($self, $viewport, $footer) = @_;
	
	my $screen_data = {
		'title' 		=> $self->title,
		'back_arrow' 	=> ($self->viewport->previous_page) ? '[<]' : '   ',
		'next_arrow' 	=> ($self->viewport->next_page)     ? '[>]' : '   ',
		'reverse'		=>	REVERSE,
		'reset' 		=>	RESET,
		'viewport'		=>	$viewport,
		'footer'		=>	$footer,
		'current_page'	=>	$self->viewport->current_page,
		'total_pages'	=>	$self->viewport->last_page,
	};

	return JMP::Template->new->render(
				$self->screen_template, 
				$screen_data
			);

}

1;
