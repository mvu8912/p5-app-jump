package JMP::Command::URL;

#------------------------------------------------------------------------------------------------
#
# URL.pm 
#
# Description:  Open a URL with a web browser
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;
use JMP::Template;

has 'help'  => <<HELP;

jmp url [url]

HELP

has 'name'  => 'url';
has 'usage' => 'open a web browser with a url';


sub run {

    my ($self, $url) = @_;

	my $open_url_template = $self->config->{commands}->{url}->{browser_command_template}
						  // "chromium-browser '[-url-]' 1>/dev/null 2>/dev/null &";

	$self->execute($open_url_template, {url => $url });
	
}

1;
