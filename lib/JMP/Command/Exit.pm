package JMP::Command::Exit;

#------------------------------------------------------------------------------------------------
#
# Back.pm 
#
# Description:  Quit.
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;

has 'help'  => <<HELP;

jmp back

HELP

has 'name'  			=> 'back';
has 'usage' 			=> 'exit back a step';


sub run {

    my ($self) = @_;
	
	exit;

}

1;
