package JMP::Command::P6;

#------------------------------------------------------------------------------------------------
#
# P6.pm 
#
# Description:  Learn things about Perl6
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;
use JMP::Template;

has 'help'  => sub {

	my ($self) = @_;
	
	my $sub_commands = join('|', sort keys %{$self->sub_commands});
	
	return <<HELP;

jmp p6 <$sub_commands>

HELP

};

has 'name'  => 'p6';
has 'usage' => 'jump to Perl6 related things';

has 'sub_commands' => sub {
	
	my ($self) = @_;
	
	my $sub_commands = $self->config->{commands}->{p6}->{sub_commands};
	
	return $sub_commands if $sub_commands;

	return {
		home 	=> 	"$0 url 'http://perl6.org'",
		irclog 	=> 	"$0 url 'http://irclog.perlgeek.de/perl6/today'",
		moarvm 	=> 	"$0 url 'http://irclog.perlgeek.de/moarvm/today'",
	};
	
};


sub run {

    my ($self, $sub_command) = @_;
	
	$sub_command = $sub_command // 'irclog';
	
    return say $self->help unless exists $self->sub_commands->{$sub_command};      

	my $command = $self->sub_commands->{$sub_command};
	
	system($command);
	
}

1;
