package JMP::Command::LS;

#------------------------------------------------------------------------------------------------
#
# LS.pm 
#
# Description:  View a jmp-able version of a directory listing
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use Cwd;
use JMP::Command -base;
use JMP::Screen;
use JMP::Screen::ActionTemplate;

# NEXT: add file globs
has 'help'  => <<HELP;

jmp ls [directory]

HELP

has 'name'  		=> 'ls';
has 'usage' 		=> 'list a directory and jmp to the files';


sub run {

    my ($self, $directory) = @_;

    $directory = $directory // getcwd();

    # search using git grep for matching lines
    my @ls_output_lines   = $self->_list_directory($directory);

    # create action templates
    my @action_templates = $self->_get_action_templates($directory, @ls_output_lines);

    # show page 1 of the results
    JMP::Screen->new(
    	title 			          =>  "jmp ls $directory",
        action_templates_height   => 1,
    	action_templates          =>  \@action_templates,
    	global_actions			  => [
    		{ text => 'back', action => "$0 back" }
    	]
    )->display_page(1);

}


sub _get_action_templates {

    my ($self, $current_directory, @ls_output_lines) = @_;

    my @action_templates;

    foreach my $ls_line (@ls_output_lines) {

        my $action;

        # match after the last modified time
        if ($ls_line =~ m{:\d\d\s(.*?/)$}) {

            my $directory = $1;

            # this is another directory
            $action = JMP::Screen::ActionTemplate->new(
                            template                =>  "[[-lowercase_key-]]     $ls_line",
                            lowercase_key_command   =>  "$0 ls $directory"
                        );

        }
        elsif ($ls_line =~ m{:\d\d\s(.*?)$}) {	# FIXME - won't work with filenames containing spaces

            my $filename = $1;

            $action = JMP::Screen::ActionTemplate->new(
                            template                =>  "[[-lowercase_key-]] [[-uppercase_key-]] $ls_line", 
                            uppercase_key_command   =>  "$0 view $filename",
                            lowercase_key_command   =>  "$0 edit $filename"
                    );

        }

        push(@action_templates, $action) if $action;

    }

    return @action_templates;

}


sub _list_directory {

	my ($self, $directory) = @_; 

    chdir($directory);

    # human readable, long, directory, flag the type
    my $command = '/bin/ls -hlaF';

	my @ls_output_lines = `$command`;

    # lose the first line of output
    shift @ls_output_lines;

    return @ls_output_lines;

}

1;
