package JMP::Command::Edit;

#------------------------------------------------------------------------------------------------
#
# Edit.pm 
#
# Description:  Invoke an $EDITOR at a particular line number
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;

has 'help'  => <<HELP;

jmp edit <filename> <line_number>

HELP

has 'name'  => 'edit';
has 'usage' => 'invoke your favourite $EDITOR';


sub run {

    my ($self, $filename, $line_number) = @_;

    return say "Usage: $0 edit <filename> [line-number]" unless $filename;      

	$line_number = $line_number // 0;

	my $editor = $self->config->get->{commands}->{edit}->{text_editor};

	return say "No text_editor found in ~/.jmp.conf config file. Please add a command template in the config file. For example: nano +[-line_number-] [-filename-]" 
		unless $editor;

	# need to test if this is a text file or a binary file
	# if this is a binary file 
	my $word_processor = $self->config->{commands}->{word_processor};

	# render the command to call the $EDITOR
	$self->execute($editor, { line_number => $line_number, filename => $filename });

}

1;
