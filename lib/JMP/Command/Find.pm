package JMP::Command::Find;

#------------------------------------------------------------------------------------------------
#
# Find.pm 
#
# Description:  git grep powered code search
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Util::Git;
use JMP::Command -base;
use JMP::Screen;
use JMP::Screen::ActionTemplate;

has 'help'  => <<HELP;

jmp find <pattern>

HELP

has 'git_root'	=> sub { JMP::Util::Git->new->git_root };
has 'name'  	=> 'find';
has 'usage' 	=> 'find a pattern in your code base';


sub run {

    my ($self, @args) = @_;

    say "Usage: $0 <pattern>" unless @args;      

    my $pattern = join(' ', @args);    

    # search using git grep for matching lines
    my @matching_lines   = $self->_get_matching_lines($pattern);

    # create action templates
    my @action_templates = $self->_get_action_templates(@matching_lines);

    # show page 1 of the results
    JMP::Screen->new(
    	title 			          =>  "jmp find $pattern",
        action_templates_height   => 3,
    	action_templates          =>  \@action_templates,
    	global_actions			  => [
    		{ text => 'back', action => "$0 back" }
    	]
    )->display_page(1);

}


sub _get_action_templates {

    my ($self, @matching_lines) = @_;

    my @action_templates;

    foreach my $matching_line (@matching_lines) {

        my ($filename, $line_number, $context) = split(/\:/, $matching_line, 3);  
		
		my $full_path = $self->git_root . '/' . $filename;

        my $template =<<TEMPLATE;
 [[-uppercase_key-]] $filename 
    [[-lowercase_key-]] (${line_number}) $context
TEMPLATE

        my $action = JMP::Screen::ActionTemplate->new(
                        template                =>  $template, 
                        uppercase_key_command   =>  "$0 view $full_path",
                        lowercase_key_command   =>  "$0 edit $full_path $line_number"
                    );

        push(@action_templates, $action);

    }

    return @action_templates;

}


sub _get_matching_lines {

    my ($self, $pattern) = @_;

	# specify file types to search - take this from config in future
    my $file_types = "'*.yml' '*.conf' '*.pm' '*.pl' '*.css' '*.html' '*.t' '*.tpl' '*.email' '*.frm' '*.page' '*.sql' '*.widget' '*.tt'";
    
    # --text - look in text files
    my $command = "git grep --text --line-number -e '" . $pattern . "' -- " . $file_types;

	my $git_root = $self->git_root;

    my @matching_lines  = `cd $git_root; $command`;

    return @matching_lines;

}


1;
