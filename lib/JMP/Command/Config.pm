package JMP::Command::Config;

#------------------------------------------------------------------------------------------------
#
# Config.pm 
#
# Description:  Edit your ~/.jmp-config file. Inspired my Mojo::Plugin::Config.
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;
use JMP::Config;
use JMP::Command::Edit;

has 'help'  => <<HELP;

jmp config

HELP

has 'name'  => 'config';
has 'usage' => 'edit your ~/.jmp.conf file';


sub run {

    my ($self) = @_;

	my $config = JMP::Config->new;
	
	$config->create_config_file 
		unless -e $config->config_location;
	
	# IDEA: check for config errors here too after editing!
	JMP::Command::Edit->new->run($config->config_location);
	
}

1;
