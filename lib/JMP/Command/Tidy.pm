package JMP::Command::Tidy;

#------------------------------------------------------------------------------------------------
#
# Tidy.pm
#
# Description:  Tidy the presentation of Perl program source code
#
# Author: 		Nigel Hamilton (nige@123.do)
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;
use JMP::Screen;
use JMP::Screen::ActionTemplate;

use Path::Tiny;
use Perl::Tidy;

has 'help' => <<HELP;

jmp tidy <perl5-filename>

HELP

has 'name'  => 'tidy';
has 'usage' => 'tidy the presentation of Perl program source code';

sub run {

    my ( $self, $perl_filename ) = @_;

    return say "Usage: $0 <perl_filename>" unless $perl_filename;
    return say "File not found: $perl_filename" unless -e $perl_filename;

    my ( $destination_string, $stderr_string, $error_file_string );

    my $argv = "-npro";                         # Ignore any .perltidyrc at this site
    $argv .= " -pbp";                           # Format according to perl best practices
    $argv .= " -nst";                           # Must turn off -st in case -pbp is specified
    $argv .= " -se";                            # -se appends the errorfile to stderr
    $argv .= " --no-blanks-before-comments";    # no blanks be --no-blanks-before-comments

    my $source_string = path($perl_filename)->slurp;

    my $error = Perl::Tidy::perltidy(
        argv        => $argv,
        source      => \$source_string,
        destination => \$destination_string,
        stderr      => \$stderr_string,

        #errorfile   => \$errorfile_string,    # ignored when -se flag is set
        ##phasers   => 'stun',                # uncomment to trigger an error
    );

    if ($error) {

        my @error_templates = $self->_get_error_templates($stderr_string);

        JMP::Screen->new(
            title                     => "jmp tidy $perl_filename",
            action_templates          => \@error_templates,
	        global_actions => [
    	        { text => 'back',     action => "$0 back"               	 },
        	    { text => 'check',    action => "$0 check    $perl_filename" },
            	{ text => 'edit',     action => "$0 edit     $perl_filename" },
            	{ text => 'podcheck', action => "$0 podcheck $perl_filename" },
            	{ text => 'perldoc',  action => "$0 perldoc  $perl_filename" },
        	],
        )->display_page(1);

    }
    else {

        # update the perl file in situ
        path($perl_filename)->spew($destination_string);

        JMP::Screen->new(
            title            => "jmp tidy $perl_filename",
            action_templates => [
                JMP::Screen::ActionTemplate->new(
                    template => 'Perl file tidied'
                )
            ],
       	 	global_actions => [
            	{ text => 'back',     action => "$0 back"              		},
	            { text => 'check',    action => "$0 check   $perl_filename" },
    	        { text => 'edit',     action => "$0 edit    $perl_filename" },
        	    { text => 'reload',   action => "$0 view    $perl_filename" },
            	{ text => 'perldoc',  action => "$0 perldoc $perl_filename" },
	        ],
        )->display_page(1);

    }

}

sub _get_error_templates {

    my ( $self, $stderr ) = @_;

    my @action_templates;

    my @stderr_lines = split( /\n/, $stderr );

    foreach my $stderr_line (@stderr_lines) {
        push(
            @action_templates,
            JMP::Screen::ActionTemplate->new(
                template => $stderr_line . "\n"
            )
        );
    }

    return @action_templates;

}

###############################################################################
#
# _run_command - execute a command and return the results
#
###############################################################################

sub _run_command {

    my ( $self, $command, $full_path ) = @_;

    # execute perl - capture errors and output
    # TODO - cross-platform /tmp files would be good too
    my $stdout_file = "/tmp/$$-tidy-stdout.jmp";
    my $stderr_file = "/tmp/$$-tidy-stderr.jmp";

    # execute the perl command
    system("$command $full_path 1>$stdout_file 2>$stderr_file");

    # pull in stdout
    my $stdout = File::Slurp::read_file($stdout_file);
    my $stderr = File::Slurp::read_file($stderr_file);

    #$self->log->notice("stdout = $stdout and stderr = $stderr");
    #$self->log->notice("contents = $stderr ");

    # remove the /tmp file
    unlink($stdout_file);
    unlink($stderr_file);

    return ( $stdout, $stderr );

}

1;
