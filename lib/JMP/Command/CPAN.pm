package JMP::Command::CPAN;

#------------------------------------------------------------------------------------------------
#
# CPAN.pm 
#
# Description:  Perform a CPAN search
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Template;
use JMP::Command -base;

has 'help'  => <<HELP;

jmp cpan <search-terms>

HELP

has 'name'  => 'cpan';
has 'usage' => 'search CPAN';


sub run {

    my ($self, @keywords) = @_;

    my $search_terms = join(' ', @keywords);

    return say $self->help unless $search_terms;      

	my $url_template = 	$self->config->{commands}->{cpan}->{url_template} 
					 || 'http://search.cpan.org/search?q=[-search_terms-]&mode=all';  


    my $search_url = JMP::Template->new->render_url(
    					$url_template,
						{ search_terms => $search_terms }
    				 );

	# jmp to the search url
	$self->execute("$0 url '[-url-]'", { url => $search_url });

}

1;
