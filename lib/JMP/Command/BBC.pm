package JMP::Command::BBC;

#------------------------------------------------------------------------------------------------
#
# BBC.pm 
#
# Description:  Jump to pages on the BBC website
#
# Author: 		Nigel Hamilton (nige@123.do) 
# Copyright: 	Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Command -base;
use JMP::Template;

has 'help'  => sub {

	my ($self) = @_;
	
	my $sub_commands = join('|', sort keys %{$self->sub_commands});
	
	return <<HELP;

jmp bbc <$sub_commands>

HELP

};

has 'name'  => 'bbc';
has 'usage' => 'jump to BBC related web pages';

has 'sub_commands' => sub {
	
	my ($self) = @_;
	
	my $sub_commands = $self->config->{commands}->{bbc}->{sub_commands};
	
	return $sub_commands if $sub_commands;

	return {
		home 	=> 	"$0 url 'http://www.bbc.co.uk'",
		news 	=> 	"$0 url 'http://www.bbc.co.uk/news'",
		weather => 	"$0 url 'http://www.bbc.co.uk/weather'",
	};
	
};


sub run {

    my ($self, $sub_command) = @_;
	
	$sub_command = $sub_command // 'news';
	
    return say $self->help unless exists $self->sub_commands->{$sub_command};      

	my $command = $self->sub_commands->{$sub_command};
	
	system($command);
	
}

1;
