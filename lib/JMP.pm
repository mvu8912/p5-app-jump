package JMP;

#------------------------------------------------------------------------------------------------
#
# JMP.pm
#
# Description:  JMP between actions and things
#
# Author:       Nigel Hamilton (nige@123.do)
# Copyright:    Artistic Licence 2.0 - 2013-NOW
#
#------------------------------------------------------------------------------------------------

use JMP::Object -base;

has 'back' => sub {

    require JMP::Command::Back;
    return JMP::Command::Back->new;

};

has 'bbc' => sub {

    require JMP::Command::BBC;
    return JMP::Command::BBC->new;

};

has 'db' => sub {

    require JMP::Command::DB;
    return JMP::Command::DB->new;

};

has 'do' => sub {

    require JMP::Command::Do;
    return JMP::Command::Do->new;

};

has check => sub {

    require JMP::Command::Check;
    return JMP::Command::Check->new;

};

has config => sub {

    require JMP::Command::Config;
    return JMP::Command::Config->new;

};

has cpan => sub {

    require JMP::Command::CPAN;
    return JMP::Command::CPAN->new;

};


has edit => sub {

    require JMP::Command::Edit;
    return JMP::Command::Edit->new;

};

has file => sub {

    require JMP::Command::File;
    return JMP::Command::File->new;

};

has git => sub {

    require JMP::Command::Git;
    return JMP::Command::Git->new;

};

has error => sub {

    require JMP::Command::Error;
    return JMP::Command::Error->new;

};

has find => sub {

    require JMP::Command::Find;
    return JMP::Command::Find->new;

};

has google => sub {

    require JMP::Command::Google;
    return JMP::Command::Google->new;

};

has help => sub {

    my ($self) = @_;

    require JMP::Command::Help;
    
    return JMP::Command::Help->new(
        commands => {
            'bbc'      	=> $self->bbc,
            'db'      	=> $self->db,
            'do'      	=> $self->do,
            'check'   	=> $self->check,
            'config'  	=> $self->config,
            'cpan'    	=> $self->cpan,
            'edit'    	=> $self->edit,
            'error'   	=> $self->error,
            'file'    	=> $self->file,
            'find'    	=> $self->find,
            'google'  	=> $self->google,
            'ls'  		=> $self->ls,
            'perldoc'  	=> $self->perldoc,
            'p6'  		=> $self->p6,
            'metacpan' 	=> $self->metacpan,
            'tidy'    	=> $self->tidy,
            'view' 	  	=> $self->view,
            'url'     	=> $self->url,
        }
    );

};


has ls => sub {

    require JMP::Command::LS;
    return JMP::Command::LS->new;

};


has metacpan => sub {

    require JMP::Command::MetaCPAN;
    return JMP::Command::MetaCPAN->new;

};


has perldoc => sub {

    require JMP::Command::PerlDoc;
    return JMP::Command::PerlDoc->new;

};


has p6 => sub {

    require JMP::Command::P6;
    return JMP::Command::P6->new;

};


has view => sub {

	# generate a view of a file we can jmp from
    require JMP::Command::View;
    return JMP::Command::View->new;

};

sub run {

    my ( $self, $command, @args ) = @_;

    return $self->help->show_all_commands
        unless $command and $self->can($command);

    $self->$command->run(@args);

}

has tidy => sub {

    require JMP::Command::Tidy;
    return JMP::Command::Tidy->new;

};

has url => sub {

    require JMP::Command::URL;
    return JMP::Command::URL->new;

};

1;
